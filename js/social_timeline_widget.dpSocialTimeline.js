/**
 * Common javascript file for Yleisurheilu Widget module.
 */

/**
 * Custom dpSocialTimeline script.
 */

(function ($, win) {

    Drupal.behaviors.socialTimelineWidget = {
        attach: function (context) {
            // Initialize dpSocialTimeline for FPP.
            var socialValues = Drupal.settings.socialTimelineWidget;
            $('#socialTimeline-12').dpSocialTimeline(socialValues);
        }
    };

})(jQuery, window);